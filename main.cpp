#include <bits/stdc++.h>
#include <ginac/ginac.h>
#include <discreture.hpp>
#include <boost/math/special_functions.hpp>

using namespace std;
using namespace GiNaC;

using boost::math::binomial_coefficient;
using boost::math::factorial;

int n, tw;

int smart_subs_time=0;

vector<vector<vector<int>>> partitions;

vector<vector<vector<symbol>>> s, t;
vector<vector<vector<symbol>>> su, tu;
vector<symbol> ss, tt;
vector<symbol> ssu, ttu;
symbol x("x");

vector<lst> shift_list;

template <
    class result_t   = std::chrono::milliseconds,
    class clock_t    = std::chrono::steady_clock,
    class duration_t = std::chrono::milliseconds
>
auto since(std::chrono::time_point<clock_t, duration_t> const& start)
{
    return std::chrono::duration_cast<result_t>(clock_t::now() - start);
}

int cycleType(const vector<int> &permutation) {
    vector<int> cycles;
    vector<bool> visited(permutation.size(), 0);

    for (int i = 0; i < permutation.size(); ++i)
        if (!visited[i]) {
            int count = 0;
            int j = i;
            while (!visited[j]) {
                visited[j] = true;
                j = permutation[j];
                ++count;
            }
            if (count > 0)
                cycles.push_back(count);
        }

    sort(cycles.begin(), cycles.end(), greater<int>());
    return find(partitions[permutation.size()].begin(), partitions[permutation.size()].end(), cycles) - partitions[permutation.size()].begin();
}

ex truncate(const ex &poly, int m) {
    ex result = 0;
    for (int i = 0; i <= m; ++i)
        result = result + pow(x, i)*poly.coeff(x, i);
    return result;
}

ex smart_subs(const ex &expr, const ex &replaced, const ex &replacement, int m) {
    auto start = chrono::steady_clock::now();
    //TODO: parallelize

    int d = expr.degree(replaced);

    vector<vector<ex> > powers_deg(d+2, vector<ex>(m+1, 0));
    vector<vector<ex> > powers_cum(d+2, vector<ex>(m+1, 0));

    for (int j = 0; j <= m; ++j)
        powers_deg[1][j] = pow(x, j)*replacement.coeff(x, j);

    for (int i = 2; i <= d; ++i)
        for (int j = 0; j <= m; ++j)
            for (int k = 0; k <= j; ++k)
                powers_deg[i][j] = powers_deg[i][j] + powers_deg[i-1][k]*powers_deg[1][j-k];

    for (int i = 1; i <= d; ++i) {
        powers_cum[i][0] = powers_deg[i][0];
        for (int j = 1; j <= m; ++j)
            powers_cum[i][j] = powers_cum[i][j-1] + powers_deg[i][j];
    }

    ex result = 0;
    for (int i = 0; i <= m; ++i) {
        ex term = expr.coeff(x, i);
        for (int j = 1; j <= term.degree(replaced); ++j)
            result = result + pow(x, i)*term.coeff(replaced, j)*powers_cum[j][m-i];
        result = result + pow(x, i)*term.coeff(replaced, 0);
    }
    
    result = expand(result);
    
    smart_subs_time += since(start).count();
    return result;
}

ex smart_pow(const ex &expr, int i, int m) {
    if (i == 0)
        return 1;
    ex aux = smart_pow(expr, i/2, m);
    ex result = aux * aux;
    if (i%2)
        result = result * expr;
    result = truncate(result, m);
    return result;
}

ex shift(const ex &expr, int j, int m) {
    return truncate(expand(expr), m/j).subs(shift_list[j]);
}

ex point(const ex &expr, int k) {
    ex result = 0;
    for (int ct = 0; ct < partitions[k].size(); ++ct)
        for (int j = 1; j <= binomial_coefficient<double>(tw, k-1)*n; ++j)
            result = result + j*t[k][ct][j]*diff(expr, s[k][ct][j]);
    return result;
}

ex point_geq2(const ex &expr, int k) {
    ex result = 0;
    for (int ct = 0; ct < partitions[k].size(); ++ct)
        for (int j = 2; j <= binomial_coefficient<double>(tw, k-1)*n; ++j)
            result = result + j*t[k][ct][j]*diff(expr, s[k][ct][j]);
    return result;
}

ex unpoint(const ex &expr, int k) {
    auto start = chrono::steady_clock::now();
    ex result = 0;
    ex act = expr;
    for (int ct = 0; ct < partitions[k].size(); ++ct)
        for (int j = 1; j <= binomial_coefficient<double>(tw, k-1)*n; ++j) {
            ex term = expand(integral(s[k][ct][j], 0, s[k][ct][j], act.coeff(t[k][ct][j], 1)).eval_integ()/j);
            act = act - point(term, k);
            result = result + term;
        }
    return result;
}

int pow_part(int k, int ct, int j) {
    vector<int> pa = partitions[k][ct];
    vector<int> parts;
    for (int p = 0; p < pa.size(); ++p)
        for (int i = 0; i < std::gcd(j, pa[p]); ++i)
            parts.push_back(pa[p]/std::gcd(j, pa[p]));
    sort(parts.begin(), parts.end(), greater<int>());
    return find(partitions[k].begin(), partitions[k].end(), parts) - partitions[k].begin();
}


int main() {
    auto start = chrono::steady_clock::now();
    
    cout << "Enter the tree-width bound and the maximum number of vertices: ";
    cin >> tw >> n;
    n = max(n, tw+1);

    lst substitutions, set_vanish, from_u, to_u;

    // Generate partitions
    partitions = vector<vector<vector<int>>>();
    partitions.push_back({}); //partition of 0
    for (int k = 1; k <= tw+1; ++k) {
        vector<vector<int>> kPartitions;
        for (auto& partition : discreture::Partitions(k))
            kPartitions.push_back(vector<int>(partition.begin(), partition.end()));
        partitions.push_back(kPartitions);
    }

    //Create variables
    s = vector<vector<vector<symbol>>> (tw+1, vector<vector<symbol>>(partitions[tw].size(), vector<symbol>(binomial_coefficient<double>(tw, tw/2)*n+1)));
    t = vector<vector<vector<symbol>>> (tw+1, vector<vector<symbol>>(partitions[tw].size(), vector<symbol>(binomial_coefficient<double>(tw, tw/2)*n+1)));
    su = vector<vector<vector<symbol>>> (tw+1, vector<vector<symbol>>(partitions[tw].size(), vector<symbol>(binomial_coefficient<double>(tw, tw/2)*n+1)));
    tu = vector<vector<vector<symbol>>> (tw+1, vector<vector<symbol>>(partitions[tw].size(), vector<symbol>(binomial_coefficient<double>(tw, tw/2)*n+1)));

    ss = vector<symbol>(n+1);
    tt = vector<symbol>(n+1);
    ssu = vector<symbol>(n+1);
    ttu = vector<symbol>(n+1);

    // Fill vectors s and t with symbols
    for (int i = 0; i < s.size(); ++i)
        for (size_t j = 0; j < s[i].size(); ++j)
            for (size_t k = 0; k < s[i][j].size(); ++k) {
                s[i][j][k] = symbol("s_" + to_string(i) + "_" + to_string(j) + "_" + to_string(k));
                t[i][j][k] = symbol("t_" + to_string(i) + "_" + to_string(j) + "_" + to_string(k));
                su[i][j][k] = symbol("su_" + to_string(i) + "_" + to_string(j) + "_" + to_string(k));
                tu[i][j][k] = symbol("tu_" + to_string(i) + "_" + to_string(j) + "_" + to_string(k));
                from_u.append(su[i][j][k] == s[i][j][k]);
                from_u.append(tu[i][j][k] == t[i][j][k]);
                to_u.append(s[i][j][k] == su[i][j][k]);
                to_u.append(t[i][j][k] == tu[i][j][k]);
            }

    // Fill vectors ss and tt with symbols
    for (int i = 0; i < ss.size(); ++i) {
        ss[i] = symbol("s_" + to_string(i));
        tt[i] = symbol("t_" + to_string(i));
        ssu[i] = symbol("su_" + to_string(i));
        ttu[i] = symbol("tu_" + to_string(i));
        from_u.append(ssu[i] == ss[i]);
        from_u.append(ttu[i] == tt[i]);
        to_u.append(ss[i] == ssu[i]);
        to_u.append(tt[i] == ttu[i]);
    }

    //Create Zset
    cout<<"set"<<endl;
    ex ZSet = 0;
    ex ZSet_pointed_geq2 = 0;
    for (int i = 0; i <= n; ++i) {
        ex term = 0;
        for (int j = 1; j <= n; ++j)
            term = term + ss[j]*pow(x, j)/j;
        term = truncate(expand(smart_pow(term, i, n)/factorial(i)), n).subs(x == 1);
        ZSet = ZSet + term;
    }
    for (int j = 2; j <= n; ++j)
        ZSet_pointed_geq2 = ZSet_pointed_geq2 + expand(j*tt[j]*diff(ZSet, ss[j]));
    cout<<"done"<<endl;

    //Define the shift list
    shift_list = vector<lst> (n+1);
    for (int j = 1; j <= n; ++j) {
        shift_list[j].append(x == pow(x, j));
        for (int i = 1; i <= tw; ++i)
            for (int k = 0; k < partitions[i].size(); ++k)
                for (int l = 1; l <= binomial_coefficient<double>(tw, i-1)*n/j; ++l) {
                    shift_list[j].append(s[i][k][l] == s[i][k][l*j]);
                    shift_list[j].append(t[i][k][l] == t[i][k][l*j]);
                }
    }

    for (int j = 1; j <= n; ++j) {
        set_vanish.append(ss[j] == 0);
        set_vanish.append(tt[j] == 0);
    }

    //precalculate alphas and kappas
    vector<vector<int> > alpha(tw+2);
    vector<vector<ex> > kappa(tw+2);
    for (int k = 1; k <= tw+1; ++k) {
        alpha[k] = vector<int> (partitions[k].size(), 0);
        kappa[k] = vector<ex> (partitions[k].size(), 0);
        for (const auto& perm : discreture::Permutations(k)) {
            int ct = cycleType(perm);
            if (alpha[k][ct]==0) {
                ex monomial = pow(x, k);
                for (int i = 1; i < k; ++i) {
                    auto cliques = discreture::combinations(k, i);
                    set<vector<int>> done;
                    for (int j = 0; j < cliques.size(); ++j)
                        if (!done.count(cliques[j])) {
                            vector<int> aux = cliques[j];
                            int l = 0;
                            sort(aux.begin(), aux.end());
                            while (!done.count(aux)) {
                                done.insert(aux);
                                for (int o = 0; o < i; ++o)
                                    aux[o] = perm[aux[o]];
                                sort(aux.begin(), aux.end());
                                ++l;
                            }

                            aux = cliques[j];
                            for (int oo = 0; oo < l; ++oo)
                                for (int o = 0; o < i; ++o)
                                    aux[o] = perm[aux[o]];

                            vector<int> perm_l(i);
                            for (int o = 0; o < i; ++o)
                                perm_l[o] = find(aux.begin(), aux.end(), cliques[j][o]) - aux.begin();

                            int ct2 = cycleType(perm_l);
                            monomial = monomial*s[i][ct2][l];
                        }
                }
                kappa[k][ct] = monomial;
            }
            ++alpha[k][ct];
        }
    }

    //define all ECIS we will use
    vector<ex> OGF(tw+1);
    vector<ex> X_G(tw+2, 0);
    vector<ex> X_G_pointed(tw+1, 0);
    vector<ex> X_G_pointed_geq2(tw+2, 0);

    vector<vector<ex>> X_GR(tw+2);
    vector<vector<ex>> X_GR1(tw+2);
    vector<vector<ex>> X_GR_pointed(tw+1);
    vector<vector<ex>> X_GR_comp_GR(tw+1);
    vector<vector<ex>> X_GR_comp_GR_pointed(tw+1);

    //starting point
    for (int ct = 0; ct < partitions[tw+1].size(); ++ct)
        X_G[tw+1] = X_G[tw+1] + alpha[tw+1][ct]*kappa[tw+1][ct]/factorial(tw+1);
    
    int pre_time = since(start).count();
    int root_time=0, recursive_time=0, pre_pointing_time=0, dissym_time=0, unpointing_time=0;

    //BIG LOOP
    for (int k = tw; k >= 1; --k) {
        auto start_iter = chrono::steady_clock::now();
        cout<<"Connectivity k="<<k<<endl;
        //calculate the k-connected class

        //step 1: root the (k+1)-connected class
        cout<<"  step 1: root the (k+1)-connected class"<<endl;
        X_GR1[k+1] = vector<ex> (partitions[k].size());
        for (int ct = 0; ct < partitions[k].size(); ++ct)
            X_GR1[k+1][ct] = expand(factorial(k)*diff(X_G[k+1], s[k][ct][1])/(alpha[k][ct]*kappa[k][ct]));
        
        root_time += since(start_iter).count();
        start_iter = chrono::steady_clock::now();

        //step 2: recursion
        cout<<"  step 2: recursion"<<endl;
        X_GR[k] = vector<ex> (partitions[k].size(), 1);
        X_GR_comp_GR[k] = vector<ex> (partitions[k].size());

        for (int iter = 0; iter <= n-k-1; ++iter) {
            cout<<"    iter "<<iter<<endl;
            cout<<"      X_GR_comp_GR"<<endl;
            for (int ct = 0; ct < partitions[k].size(); ++ct) {
                X_GR_comp_GR[k][ct] = X_GR1[k+1][ct].subs(to_u);
                for (int ct2 = 0; ct2 < partitions[k].size(); ++ct2)
                    for (int j = 1; j <= n-k; ++j)
                        X_GR_comp_GR[k][ct] = smart_subs(X_GR_comp_GR[k][ct], su[k][ct2][j], expand(s[k][ct2][j]*shift(X_GR[k][ct2], j, n-k)), n-k);
                X_GR_comp_GR[k][ct] = X_GR_comp_GR[k][ct].subs(from_u);
            }

            cout<<"      X_GR"<<endl;
            for (int ct = 0; ct < partitions[k].size(); ++ct) {
                X_GR[k][ct] = ZSet.subs(to_u);
                for (int j = 1; j <= n-k; ++j)
                    X_GR[k][ct] = smart_subs(X_GR[k][ct], ssu[j], shift(X_GR_comp_GR[k][pow_part(k, ct, j)], j, n-k), n-k);
                X_GR[k][ct] = X_GR[k][ct].subs(from_u).subs(set_vanish);
            }
        }
        
        recursive_time += since(start_iter).count();
        start_iter = chrono::steady_clock::now();

        //step 3: we prepare some cycle-pointed sums we will need
        cout<<"  step 3: we prepare some cycle-pointed sums we will need"<<endl;
        X_G_pointed_geq2[k+1] = point_geq2(X_G[k+1], k);

        X_GR_pointed[k] = vector<ex>(partitions[k].size(), 0);
        X_GR_comp_GR_pointed[k] = vector<ex>(partitions[k].size(), 0);
        for (int ct = 0; ct < partitions[k].size(); ++ct) {
            X_GR_pointed[k][ct] = point(X_GR[k][ct], k);
            X_GR_comp_GR_pointed[k][ct] = point(X_GR_comp_GR[k][ct], k);
        }
        
        pre_pointing_time += since(start_iter).count();
        start_iter = chrono::steady_clock::now();

        //step 4: we compute the cycle-pointed unrooted class:
        cout<<"  step 4: we compute the cycle-pointed unrooted class"<<endl;

        cout<<"    4.1"<<endl;
        X_G_pointed_geq2[k] = X_G_pointed_geq2[k+1].subs(to_u);
        for (int ct = 0; ct < partitions[k].size(); ++ct)
            for (int j = 1; j <= n-k; ++j) {
                X_G_pointed_geq2[k] = smart_subs(X_G_pointed_geq2[k], su[k][ct][j], expand(shift(s[k][ct][1]*X_GR[k][ct], j, n-k)), n);
                X_G_pointed_geq2[k] = smart_subs(X_G_pointed_geq2[k], tu[k][ct][j], expand(shift(t[k][ct][1]*X_GR[k][ct] + s[k][ct][1]*X_GR_pointed[k][ct], j, n-k)), n);
            }
        X_G_pointed_geq2[k] = X_G_pointed_geq2[k].subs(from_u);

        

        cout<<"    4.2"<<endl;
        for (int ct = 0; ct < partitions[k].size(); ++ct) {
            ex aux = ZSet_pointed_geq2.subs(to_u);
            substitutions = lst();
            for (int j = 1; j <= n-k; ++j) {
                aux = smart_subs(aux, ssu[j], shift(X_GR_comp_GR[k][pow_part(k, ct, j)], j, n-k), n);
                aux = smart_subs(aux, ttu[j], shift(X_GR_comp_GR_pointed[k][pow_part(k, ct, j)], j, n-k), n);
            }
            X_G_pointed_geq2[k] = X_G_pointed_geq2[k] + truncate(expand(kappa[k][ct]*alpha[k][ct]*s[k][ct][1]*aux.subs(from_u).subs(set_vanish)/factorial(k)), n);
        }
        X_G_pointed[k] = X_G_pointed_geq2[k];

        cout<<"    4.3"<<endl;
        for (int ct = 0; ct < partitions[k].size(); ++ct)
            X_G_pointed[k] = X_G_pointed[k] + truncate(expand(kappa[k][ct]*alpha[k][ct]*t[k][ct][1]*X_GR[k][ct]/factorial(k)), n);


        
        dissym_time += since(start_iter).count();
        start_iter = chrono::steady_clock::now();
        
        
        
        //step 5: unpoint and show
        cout<<"  step 5: unpoint and show"<<endl;
        X_G[k] = unpoint(X_G_pointed[k], k);

        substitutions = lst();
        for (int ct = 0; ct < partitions[k].size(); ++ct)
            for (int j = 1; j <= binomial_coefficient<double>(tw, k-1)*n; ++j)
                substitutions.append(s[k][ct][j] == 1);
        X_G[k] = X_G[k].subs(substitutions);

        substitutions = lst();
        for (int i = 1; i < k; ++i)
            for (int ct = 0; ct < partitions[i].size(); ++ct)
                for (int j = 1; j <= binomial_coefficient<double>(tw, i-1)*n; ++j)
                    substitutions.append(s[i][ct][j] == 1);
        OGF[k] = X_G[k].subs(substitutions);

        cout<<"OFG for k="<<k<<endl;
        cout<<OGF[k]<<endl;
        cout<<"coeffs: ";
        for (int i = 1; i <= n; ++i)
            cout<<OGF[k].coeff(x, i)<<" ";
        cout<<endl;
        
        
        
        unpointing_time += since(start_iter).count();
    }
    
    cout<<"TABLE OF COEFFICIENTS"<<endl;
    for (int k = tw; k >= 1; --k) {
        for (int i = 1; i <= n; ++i)
            cout<<OGF[k].coeff(x, i)<<" ";
        cout<<endl;
    }
    
    cout << "Elapsed(ms)=" << since(start).count() << endl;
    cout<<"Smart substitution: "<<100*smart_subs_time/since(start).count()<<"%"<<endl;
    cout<<"Preprocessing: "<<100*pre_time/since(start).count()<<"%"<<endl;
    cout<<"Step 1: root: "<<100*root_time/since(start).count()<<"%"<<endl;
    cout<<"Step 2: recursion: "<<100*recursive_time/since(start).count()<<"%"<<endl;
    cout<<"Step 3: prepare pointed: "<<100*pre_pointing_time/since(start).count()<<"%"<<endl;
    cout<<"Step 4: dissym: "<<100*dissym_time/since(start).count()<<"%"<<endl;
    cout<<"Step 5: unpoint: "<<100*unpointing_time/since(start).count()<<"%"<<endl;
    return 0;
}
