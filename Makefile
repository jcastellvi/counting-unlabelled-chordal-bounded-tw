CXX = g++
CXXFLAGS = -std=c++17 -Wall -Wextra -O2
INCLUDES = -I/usr/include
LIBS = -lboost_math_c99 -lginac

SRCS = main.cpp

OBJS = $(SRCS:.cpp=.o)

TARGET = count

all: $(TARGET)

$(TARGET): $(OBJS)
	$(CXX) $(CXXFLAGS) $(INCLUDES) -o $@ $^ $(LIBS)

%.o: %.cpp
	$(CXX) $(CXXFLAGS) $(INCLUDES) -c -o $@ $<

sanitize: CXXFLAGS += -fsanitize=undefined,address
sanitize: all

clean:
	$(RM) $(OBJS) $(TARGET)
